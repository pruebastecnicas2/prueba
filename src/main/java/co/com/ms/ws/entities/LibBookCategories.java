package co.com.ms.ws.entities;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Data
@Entity
@Table(name= "LIB_BOOK_CATEGORIES", schema = "public")
public class LibBookCategories {

	@Id	
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_cat")
    @SequenceGenerator(name = "seq_cat", sequenceName = "seq_cat", initialValue = 1, allocationSize = 1)	
	@Column(name = "CAT_ID")
	private long CatId;
	@NotEmpty(message = "La description name no puede estar vacio o ser nulo")
	@Column(name="CAT_DESCRIPCION")
	private String CatDescription;
}
