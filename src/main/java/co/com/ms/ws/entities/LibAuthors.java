package co.com.ms.ws.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Data
@Entity
@Table(name= "LIB_AUTHORS", schema = "public")
public class LibAuthors {
	
	@Id	
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_authors")
    @SequenceGenerator(name = "seq_authors", sequenceName = "seq_authors", initialValue = 1, allocationSize = 1)	
	@Column(name = "AUT_ID")
	private long AutId;
	@NotEmpty(message = "El first name no puede estar vacio o ser nulo")
	@Column(name="AUT_FIRST_NAME")
	private String AutFirstName;
	@NotEmpty(message = "El last name no puede estar vacio o ser nulo")
	@Column(name="AUT_LAST_NAME")
	private String AutLastName;
	@Column(name="AUT_DATE_DOB")
	private Date AutDateDob;
	@NotEmpty(message = "El gender no puede estar vacio o ser nulo")
	@Column(name="AUT_GENDER")
	private String AutGender;
	@NotEmpty(message = "El contact no puede estar vacio o ser nulo")
	@Column(name="AUT_CONTACT")
	private String AutContact;
	@NotEmpty(message = "El other details no puede estar vacio o ser nulo")
	@Column(name="AUT_OTHER_DETAILS")
	private String AutOtherDetails;
	

}
