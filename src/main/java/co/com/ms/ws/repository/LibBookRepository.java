package co.com.ms.ws.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import co.com.ms.ws.entities.LibBookCategories;
import co.com.ms.ws.entities.LibBooks;
import java.util.List;


public interface LibBookRepository extends JpaRepository<LibBooks, Long> {
	
	List<LibBooks> findByBookCatId(LibBookCategories bookCatId);

}
