package co.com.ms.ws.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import co.com.ms.ws.entities.LibBookCategories;

public interface LibBookCategoriesRepository extends JpaRepository<LibBookCategories, Long>{

}
