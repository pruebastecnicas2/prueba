package co.com.ms.ws.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import co.com.ms.ws.entities.LibAuthors;

public interface LibAuthorsRepository extends JpaRepository<LibAuthors, Long>{

}
