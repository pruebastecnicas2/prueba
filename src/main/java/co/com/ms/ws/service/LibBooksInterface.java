package co.com.ms.ws.service;

import java.util.List;

import co.com.ms.ws.dto.LibBooksDTO;

public interface LibBooksInterface {

	List<LibBooksDTO> findByBookCatId(long idCategoria);
}
