package co.com.ms.ws.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.com.ms.ws.dto.LibBooksDTO;
import co.com.ms.ws.entities.LibBookCategories;
import co.com.ms.ws.entities.LibBooks;
import co.com.ms.ws.repository.LibBookRepository;

@Service
public class LibBooksImp implements LibBooksInterface {

	@Autowired
	LibBookRepository libBookRepository;
	
	private Logger log = Logger.getLogger("Log Books");
	
	public List<LibBooksDTO> findByBookCatId(long idCategoria) {		
		log.log(Level.INFO, "Inicio busqueda de libro");
		List<LibBooksDTO> libBooksArray = new ArrayList<>();
		LibBookCategories categoria = new LibBookCategories();
		categoria.setCatId(idCategoria);
		List<LibBooks> libBookFind = libBookRepository.findByBookCatId(categoria);	
		
		for (LibBooks libBooksEach: libBookFind) {
			LibBooksDTO libBooksDto = LibBooksDTO.builder()
				.BookId(libBooksEach.getBookId())
				.bookAutId(libBooksEach.getBookAutId())
				.bookCatId(libBooksEach.getBookCatId())
				.BookIsnb(libBooksEach.getBookIsnb())
				.BookPublishedDate(libBooksEach.getBookPublishedDate())
				.BookTitle(libBooksEach.getBookTitle())
				.BookPrice(libBooksEach.getBookPrice())
				.BookDescription(libBooksEach.getBookDescription())
				.BookImage(libBooksEach.getBookImage())
				.BookMimetype(libBooksEach.getBookMimetype())
				.BookFilename(libBooksEach.getBookFilename())
				.BookImageLastUpdate(libBooksEach.getBookImageLastUpdate())
				.build();
			libBooksArray.add(libBooksDto);
		}
		log.log(Level.INFO, "Termina Metodo");
		return libBooksArray;
		
	}
		
}
