package co.com.ms.ws.ui.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import co.com.ms.ws.dto.LibBooksDTO;
import co.com.ms.ws.exception.EntityServiceException;
import co.com.ms.ws.service.LibBooksInterface;
import co.com.ms.ws.ui.model.response.Response;

@RequestMapping(path = "${controller.properties.base-path}") 
@RestController
public class LibAuthorsController {

	@Autowired
	private LibBooksInterface libBookInterface;
	
	@GetMapping(value = "/findBooksByCategoriesId")
	public Response findBooksCatId(@RequestParam long categoriaId) {
		try {
			List<LibBooksDTO> libBooksDTOFind = libBookInterface.findByBookCatId(categoriaId);
			return new Response(HttpStatus.OK.value(), "Listado Exitoso", libBooksDTOFind);	
		} catch (EntityServiceException exception) {
			return new Response(HttpStatus.NO_CONTENT.value(), HttpStatus.BAD_REQUEST.name(), "No hay Datos con el parametro enviado");
		}
	}
}
