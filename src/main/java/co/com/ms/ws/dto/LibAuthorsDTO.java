package co.com.ms.ws.dto;

import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Setter
@Getter
@AllArgsConstructor()
public class LibAuthorsDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private long AutId;
	private String AutFirstName;
	private String AutLastName;
	private Date AutDateDob;
	private String AutGender;
	private String AutContact;
	private String AutOtherDetails;	
	
	public LibAuthorsDTO() {
		
	}

}
