package co.com.ms.ws.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Setter
@Getter
@AllArgsConstructor()
public class LibBookCategoriesDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private long CatId;
	private String CatDescription;
	
	public LibBookCategoriesDTO() {
		
	}

}
