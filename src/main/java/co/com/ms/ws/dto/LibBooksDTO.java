package co.com.ms.ws.dto;

import java.io.Serializable;
import java.util.Date;

import co.com.ms.ws.entities.LibAuthors;
import co.com.ms.ws.entities.LibBookCategories;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Setter
@Getter
@AllArgsConstructor()
public class LibBooksDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private long BookId;
	private String BookIsnb;
	private Date BookPublishedDate;
	private String BookTitle;
	private float BookPrice;
	private String BookDescription;
	private String BookImage;
	private String BookMimetype;
	private String BookFilename;
	private Date BookImageLastUpdate;
	private LibAuthors bookAutId;
	private LibBookCategories bookCatId;
	
	public LibBooksDTO() {
		
	}

}
